import tensorflow as tf
import numpy as np
import math
import tensorflow.contrib.integrate as it

class ecg_sim:

    def __init__(self):
        # self.s = tf.Session()

        # General consts:
        self.PI = math.pi
        self.NTAB = 32
        self.IQ = 127773
        self.IR = 2836
        self.IA = 16807
        self.IM = 2147483647
        self.NDIV = (1 + (self.IM - 1) / self.NTAB)
        self.EPS = 1.2e-7
        self.RNMX = (1.0 - self.EPS)
        self.AM = (1.0 / self.IM)

        # Variables for the static function ran1()
        self.iy = 0
        self.iv = [0 for x in range(32)]

        # Values relevant to the rr-process generation: ( I hope it doesnt depand on the learning params):
        self.sfecg = 256.0  # ECG sampling frequency
        self.sf = 512.0  # Internal sampling frequency
        self.hr_mean = 60  # Heart rate mean.
        self.h = 1.0 / float(self.sf)
        self.n = 2  # 256  # Approximate number of heart beats.
        self.rr = None  # The rr process.
        self.rrpc = None # The rr process pice wise
        self.lfhfratio = 0.5
        self.hrstd = 1.0

        # Define frequency parameters for rr process:
        # flo and fhi correspond to the Mayer waves and respiratory rate respectively:
        self.flo = 0.1
        self.fhi = 0.25
        self.flostd = 0.01
        self.fhistd = 0.01

        self.rseed = -1  # Initialise seed.

        # Define learning parmas:
        self.theta = [0.0 for _ in range(6)]
        self.a = [0.0 for _ in range(6)]
        self.b = [0.0 for _ in range(6)]

        self.theta[1] = -60.0 * self.PI / 180.0
        self.theta[2] = -15.0 * self.PI / 180.0
        self.theta[3] = 0.0 * self.PI / 180.0
        self.theta[4] = 15.0 * self.PI / 180.0
        self.theta[5] = 90.0 * self.PI / 180.0

        self.a[1] = 1.2
        self.a[2] = -5.0
        self.a[3] = 30.0
        self.a[4] = -7.5
        self.a[5] = 0.75

        self.b[1] = 0.25
        self.b[2] = 0.1
        self.b[3] = 0.1
        self.b[4] = 0.1
        self.b[5] = 0.4

        # convert to tensorflow:
        self.a = tf.get_variable("a", shape=[6], initializer=tf.constant_initializer(self.a))
        self.b = tf.get_variable("b", shape=[6], initializer=tf.constant_initializer(self.b))
        self.theta = tf.get_variable("theta", shape=[6], initializer=tf.constant_initializer(self.theta))

        # Initial values:
        # Declare state vector:
        ''''
        self.x = [0.0 for e in range(4)]  # new double[4]

        # Initialise the vector
        self.x[1] = 1.0
        self.x[2] = 0.0
        self.x[3] = 0.04
        self.x = tf.get_variable("init_values", shape=[4], initializer=tf.constant_initializer(self.x))
        '''
        '''
        self.x = [tf.Variable(0.0, name='x_0'), tf.Variable(1.0, name='x_1'), tf.Variable(0.0, name='x_2'),
                  tf.Variable(0.04, name='x_3')]
        '''
        self.x_initial = tf.Variable(1.0, name='x_initial')
        self.y_initial = tf.Variable(0.0, name='y_initial')
        self.z_initial = tf.Variable(0.04, name='z_initial')

    def run_ecg_calcs(self):
        """
        Run part of program.
        :return:
        """
        RetValue = True

        # step 1 : Perform some checks on input values:
        q = int(np.rint(self.sf / self.sfecg))


        # Step 2: Convert degress to radians (Already done that in the init part )

        # Step 3 : Adjust extrema parameters for mean heart rate:
        hrfact = math.sqrt(self.hr_mean / 60)
        hrfact2 = math.sqrt(hrfact)

        self.b_adjusted_to_mean_heart_rate = self.b * tf.constant(hrfact)

        '''
        self.theta_i[1] *= hrfact2
        self.theta_i[2] *= hrfact
        self.theta_i[3] *= 1.0
        self.theta_i[4] *= hrfact
        self.theta_i[5] *= 1.0
        '''

        # Step 3: Calculate time scales:
        tstep = 1.0 / float(self.sfecg)

        # Calculate length of RR time series:
        rrmean = (60.0 / self.hr_mean)
        number_of_rrs = int(math.pow(2.0, math.ceil(math.log(self.n * rrmean * self.sf) / math.log(2.0))))

        # Step 4: Create rrprocess with required spectrum:
        self.rr = [0 for e in range(number_of_rrs + 1)]  # new double[Nrr + 1]

        self.rrprocess(self.rr, self.flo, self.fhi, self.flostd,
                       self.fhistd, self.lfhfratio, self.hr_mean,
                       self.hrstd, self.sf, number_of_rrs)

        # Step 5: Create piecewise constant rr:
        self.rrpc = [0 for e in range((2 * number_of_rrs) + 1)]  # new double[(2 * Nrr) + 1];
        tecg = 0.0
        i = 1
        j = 1
        while i <= number_of_rrs:
            tecg += self.rr[j]
            j = int(np.rint(tecg / self.h))

            for k in range(i, j + 1):
                self.rrpc[k] = self.rr[i]
            i = j + 1

        Nt = j  # 131543.
        print(Nt)
        # At this point RR-process is done.
        timev = [0.0]
        '''
        for i in range(1, Nt + 1):
            timev.append(timev[-1] + self.h)
        self.t = timev
        self.rk4_tf()
        '''

        # Step 6 : Integrate dynamical system using fourth order Runge-Kutta:
        # xt = [tf.constant(0.0) for e in range(Nt + 1)]  # new double[Nt + 1];
        # yt = [tf.constant(0.0) for e in range(Nt + 1)]  # new double[Nt + 1];
        # zt = [tf.constant(0.0) for e in range(Nt + 1)]  # new double[Nt + 1];
        xt = [tf.constant(0.0)]
        yt = [tf.constant(0.0)]
        zt = [tf.constant(0.0)]
        timev = 0.0

        x_out = self.x_initial
        y_out = self.y_initial
        z_out = self.z_initial
        for i in range(1, Nt + 1):
            if i % 10 == 0:
                print("iter:", i)
            x_in = x_out
            y_in = y_out
            z_in = z_out
            xt = tf.concat([xt, [x_in]], 0)
            yt = tf.concat([yt, [y_in]], 0)
            zt = tf.concat([zt, [z_in]], 0)
            x_out, y_out, z_out = self.RK4(x_in, y_in, z_in, timev, self.h)  #  x is the starting values of x0 y0 z0 variables. (4d) first is always zero.
            timev += self.h

        print("DONE 1")
        s = tf.Session()
        s.run(tf.global_variables_initializer())
        print(s.run(xt))
        grs = tf.gradients(xt, self.a)
        print("calc grads:")
        print(s.run(grs))
        '''
        # Step 7: Downsample to ECG sampling frequency:
        xts = [tf.constant(0.0)]  # new double[Nt + 1];
        yts = [tf.constant(0.0)] # new double[Nt + 1];
        zts = [tf.constant(0.0)]  # new double[Nt + 1];

        j = 0
        i = 1
        while i <= Nt:
            j += 1
            xts = tf.concat([xts, [xt[i]]], 0)
            yts = tf.concat([yts, [yt[i]]], 0)
            zts = tf.concat([zts, [zt[i]]], 0)
            i += q


        # Step 8: Do peak detection using angle
        Nts = j
        ipeak = [0 for e in range(Nts + 1)]  # new double[Nts + 1];
        # self.detect_peaks(ipeak, xts, yts, zts, Nts)

        
        # Scale signal to lie between -0.4 and 1.2 mV :
        zmin = zts[1]
        zmax = zts[1]

        for i in range(2, Nts + 1):
            if zts[i] < zmin:
                zmin = zts[i]
            elif zts[i] > zmax:
                zmax = zts[i]

        zrange = zmax - zmin
        # for (i=1; i <= Nts; i++)

        for i in range(1, Nts + 1):
            zts[i] = (zts[i] - zmin) * (1.6) / zrange - 0.4

        # Include additive uniformly distributed measurement noise:
        # for (i=1; i <= Nts; i++)
        for i in range(1, Nts + 1):
            zts[i] += self.ecg_params.getANoise() * (2.0 * self.ran1() - 1.0)

        # Insert into the ECG data table:
        print("Generating result matrix...")

        self.ecg_result_num_rows = Nts

        self.ecg_result_time = [0 for e in range(self.ecg_result_num_rows)]
        self.ecg_result_voltage = [0 for e in range(self.ecg_result_num_rows)]
        self.ecg_result_peaks = [0 for e in range(self.ecg_result_num_rows)]

        # for (i=1;i <= Nts;i++){
        for i in range(1, Nts + 1):
            self.ecg_result_time[i - 1] = (i - 1) * tstep
            self.ecg_result_voltage[i - 1] = zts[i]
            self.ecg_result_peaks[i - 1] = int(ipeak[i])

        print("Finished generating result matrix.")
        return (RetValue)
    '''

    def RK4(self, x_in, y_in, z_in, timev, h):
        """
        y and yout are always the same.
        n - always 3.
        x is timev and it changes.
        h is always some constant.
        RUNGA-KUTTA FOURTH ORDER INTEGRATION
        :param y:
        :param n:
        :param x:
        :param h:
        :param yout:
        :return:
        """
        k1_x, k1_y, k1_z = self.derivs_pqrst_calc(timev, x_in, y_in, z_in)

        x_in_2 = x_in + tf.constant(h * 0.5) * k1_x
        y_in_2 = y_in + tf.constant(h * 0.5) * k1_y
        z_in_2 = z_in + tf.constant(h * 0.5) * k1_z

        k2_x, k2_y, k2_z = self.derivs_pqrst_calc(timev + h * 0.5, x_in_2, y_in_2, z_in_2)

        x_in_3 = x_in + tf.constant(h * 0.5) * k2_x
        y_in_3 = y_in + tf.constant(h * 0.5) * k2_y
        z_in_3 = z_in + tf.constant(h * 0.5) * k2_z

        k3_x, k3_y, k3_z = self.derivs_pqrst_calc(timev + h * 0.5, x_in_3, y_in_3, z_in_3)

        x_in_4 = x_in + tf.constant(h) * k3_x
        y_in_4 = y_in + tf.constant(h) * k3_y
        z_in_4 = z_in + tf.constant(h) * k3_z

        k4_x, k4_y, k4_z = self.derivs_pqrst_calc(timev + h, x_in_4, y_in_4, z_in_4)

        x_out = x_in + tf.constant(h / 6.0) * (k1_x + 2 * k2_x + 2 * k3_x + k4_x)
        y_out = y_in + tf.constant(h / 6.0) * (k1_y + 2 * k2_y + 2 * k3_y + k4_y)
        z_out = z_in + tf.constant(h / 6.0) * (k1_z + 2 * k2_z + 2 * k3_z + k4_z)
        return x_out, y_out, z_out

    def derivs_pqrst_calc(self, t0, x_in, y_in, z_in):
        """
        THE EXACT NONLINEAR DERIVATIVES
        :param t0:
        :param x:
        :param dxdt:
        :return:
        """
        w0 = self.ang_freq_calc(t0)
        a0 = tf.constant(1.0) - tf.sqrt(x_in * x_in + y_in * y_in)

        zbase = 0.005 * math.sin(2.0 * self.PI * self.fhi * t0)

        theta = tf.atan2(y_in, x_in)
        dx_dt = a0 * x_in - tf.constant(w0) * y_in
        dy_dt = a0 * y_in + tf.constant(w0) * x_in

        d_theta_p = tf.mod(theta - self.theta[1], tf.constant(2.0 * self.PI))  # TODO: change mod
        delta_theta_2_p = d_theta_p * d_theta_p
        dz_dt_p = tf.constant(-1.0) * self.a[1] * d_theta_p * tf.exp(tf.constant(-0.5) * delta_theta_2_p /
                                                                     (self.b_adjusted_to_mean_heart_rate[1] *
                                                                      self.b_adjusted_to_mean_heart_rate[1]))

        d_theta_q = tf.mod(theta - self.theta[2], tf.constant(2.0 * self.PI))  # TODO: change mod
        delta_theta_2_q = d_theta_q * d_theta_q
        dz_dt_q = tf.constant(-1.0) * self.a[2] * d_theta_q * tf.exp(tf.constant(-0.5) * delta_theta_2_q /
                                                                     (self.b_adjusted_to_mean_heart_rate[2] *
                                                                      self.b_adjusted_to_mean_heart_rate[2]))

        d_theta_r = tf.mod(theta - self.theta[3], tf.constant(2.0 * self.PI))  # TODO: change mod
        delta_theta_2_r = d_theta_r * d_theta_r
        dz_dt_r = tf.constant(-1.0) * self.a[3] * d_theta_r * tf.exp(tf.constant(-0.5) * delta_theta_2_r /
                                                                     (self.b_adjusted_to_mean_heart_rate[3] *
                                                                      self.b_adjusted_to_mean_heart_rate[3]))

        d_theta_s = tf.mod(theta - self.theta[4], tf.constant(2.0 * self.PI))  # TODO: change mod
        delta_theta_2_s = d_theta_s * d_theta_s
        dz_dt_s = tf.constant(-1.0) * self.a[4] * d_theta_s * tf.exp(tf.constant(-0.5) * delta_theta_2_s /
                                                                     (self.b_adjusted_to_mean_heart_rate[4] *
                                                                      self.b_adjusted_to_mean_heart_rate[4]))

        d_theta_t = tf.mod(theta - self.theta[5], tf.constant(2.0 * self.PI))  # TODO: change mod
        delta_theta_2_t = d_theta_t * d_theta_t
        dz_dt_t = tf.constant(-1.0) * self.a[5] * d_theta_t * tf.exp(tf.constant(-0.5) * delta_theta_2_t /
                                                                     (self.b_adjusted_to_mean_heart_rate[5] *
                                                                      self.b_adjusted_to_mean_heart_rate[5]))

        dz_dt = dz_dt_p + dz_dt_q + dz_dt_r + dz_dt_s + dz_dt_t + tf.constant(-1.0) * (z_in - tf.constant(zbase))

        return dx_dt, dy_dt, dz_dt

    def ang_freq_calc(self, t):
        """
        THE ANGULAR FREQUENCY
        :param t:
        :return:
        """

        i = 1 + int(math.floor(t / self.h))

        if self.rrpc[i] == 0:
            return math.inf
        return 2.0 * self.PI / float(self.rrpc[i])

    def rrprocess(self, rr, flo, fhi, flostd, fhistd, lfhfratio, hrmean, hrstd, sf, n):
        """
        GENERATE RR PROCESS
        :param rr:
        :param flo:
        :param fhi:
        :param flostd:
        :param fhistd:
        :param lfhfratio:
        :param hrmean:
        :param hrstd:
        :param sf:
        :param n:
        :return:
        """

        w = [0 for e in range(n+1)]  # new double[n + 1];
        Hw = [0 for e in range(n+1)]
        Sw = [0 for e in range(n+1)]
        ph0_len = int(n / 2.0 - 1 + 1)
        ph0 = [0 for e in range(ph0_len)]  # new double[(int)(n / 2 - 1 + 1)];
        ph = [0 for e in range(n+1)]
        SwC = [0 for e in range(2 *n + 1)]  # new double[(2 * n) + 1];

        w1 = 2.0 * self.PI * flo
        w2 = 2.0 * self.PI * fhi
        c1 = 2.0 * self.PI * flostd
        c2 = 2.0 * self.PI * fhistd
        sig2 = 1.0
        sig1 = lfhfratio
        rrmean = 60.0 / hrmean
        rrstd = 60.0 * hrstd / (hrmean * hrmean)

        df = sf / float(n)

        for i in range(1, n+1):
            w[i] = (i - 1) * 2.0 * self.PI * df

        for i in range(1, n + 1):
            Hw[i] = (sig1 * math.exp(-0.5 * (math.pow(w[i] - w1, 2) / math.pow(c1, 2))) /
                     math.sqrt(2 * self.PI * c1 * c1)) + \
                    (sig2 * math.exp(-0.5 * (math.pow(w[i]-w2, 2)/math.pow(c2, 2))) / math.sqrt(2 * self.PI*c2*c2))

        for i in range(1, int(n/2) + 1):
            Sw[i] = (sf / 2.0) * math.sqrt(Hw[i])

        for i in range(int(n/2) + 1, n + 1):
            Sw[i] = (sf / 2.0) * math.sqrt(Hw[n - i + 1])

        # randomise the phases :
        for i in range(1, int(n/2)):
            ph0[i] = 2.0 * self.PI * self.ran1()

        ph[1] = 0.0
        for i in range(1, int(n/2)):
            ph[i + 1] = ph0[i]

        ph[int(n / 2) + 1] = 0.0
        for i in range(1, int(n/2)):
            ph[n - i + 1] = - ph0[i]

        # make complex spectrum :
        for i in range(1, n + 1):
            SwC[2 * i - 1] = Sw[i] * math.cos(ph[i])

        for i in range(1, n + 1):
            SwC[2 * i] = Sw[i] * math.sin(ph[i])

        # Calclate inverse fft:
        self.ifft(SwC, n, -1)

        # Excecute real part :
        for i in range(1, n + 1):
            rr[i] = (1.0 / float(n))*SwC[2 * i - 1]

        xstd = self.stdev_calc(rr, n)
        ratio = rrstd / xstd

        for i in range(1, n + 1):
            rr[i] *= ratio

        for i in range(1, n + 1):
            rr[i] += rrmean

    def ran1(self):
        """
        TODO: Give a better name
        :return:
        """
        if self.iy == 0:
            flg = False
        else:
            flg = True

        if self.rseed <= 0 or not flg:
            if -1 * self.rseed < 1:
                self.rseed = 1
            else:
                self.rseed = -1 * self.rseed

            j = self.NTAB + 7

            while j >= 0:
                k = int(self.rseed / self.IQ)
                self.rseed = self.IA * (self.rseed - k * self.IQ) - self.IR * k
                if self.rseed < 0:
                    self.rseed += self.IM
                if j < self.NTAB:
                    self.iv[j] = self.rseed
                j -= 1
            self.iy = self.iv[0]

        k = int(self.rseed / self.IQ)
        self.rseed = self.IA * ( self.rseed - k * self.IQ) - self.IR * k

        if self.rseed < 0:
            self.rseed += self.IM

        j = int(float(self.iy) / float(self.NDIV))
        self.iy = self.iv[j]
        self.iv[j] = self.rseed

        if (self.AM * self.iy) > self.RNMX:
            return self.RNMX
        else:
            return self.AM * self.iy

    def ifft(self, data, nn, isign):
        """

        :param data:
        :param nn:
        :param isign:
        :return:
        """
        n = nn * 2
        j = 1
        i = 1
        print("length of data: ", len(data))
        print("Value of N", n)
        while i < n:
            if j > i:
                # print("j: ", j)
                swap = data[int(j)]
                data[int(j)] = data[int(i)]
                data[int(i)] = swap

                swap = data[int(j) + 1]
                data[int(j) + 1] = data[int(i) + 1]
                data[int(i) + 1] = swap
            m = n >> 1
            if j == 1:
                print("m:", m)
            while m >= 2 and j > m:
                j -= m
                m >>= 1
            j += m

            i += 2

        mmax = 2

        while n > mmax:
            istep = mmax * 2
            theta = isign * (6.28318530717959/float(mmax))
            wtemp = math.sin(0.5 * theta)
            wpr = -2.0 * wtemp * wtemp
            wpi = math.sin(theta)
            wr = 1.0
            wi = 0.0

            m = 1
            while m < mmax:
                i = m
                while i <= n:
                    j = i + mmax
                    tempr = wr * data[int(j)] - wi * data[int(j)+ 1]
                    tempi = wr * data[int(j) + 1] + wi * data[int(j)]

                    data[int(j)] = data[int(i)] - tempr
                    data[int(j) + 1] = data[int(i) + 1] - tempi
                    data[int(i)] += tempr
                    data[int(i) + 1] += tempi

                    i += istep
                wtemp = wr
                wr = wtemp * wpr - wi * wpi + wr
                wi = wi * wpr + wtemp * wpi + wi

                m += 2
            mmax = istep


    def stdev_calc(self, x, n):
        """

        :param x:
        :param n:
        :return:
        """
        add = 0.0
        j = 1
        while j <= n:
            add += x[j]
            j += 1
        mean = float(add) / float(n)

        total = 0.0
        for j in range(1, n + 1):
            diff = x[j] - mean
            total += diff * diff

        return math.sqrt(float(total) / float(n - 1))


    def rk4_tf(self):
        func_drivatives = lambda y, t: self.tf_pqrst_drivatives(y, t)
        # Initialise the vector
        y0 = [1.0, 0.0, 0.04]
        y0 = tf.get_variable("init_values", shape=[3], initializer=tf.constant_initializer(y0))
        self.rrpc_tf = tf.get_variable("rrpc", shape=[len(self.rrpc)], initializer=tf.constant_initializer(self.rrpc))
        sol = it.odeint(func_drivatives, y0, self.t)
        return sol

    def tf_pqrst_drivatives(self, y, t):
        w0 = self.tf_ang_freq_calc(t)
        a0 = tf.constant(1.0) - tf.sqrt(y[0] * y[0] + y[1] * y[1])

        zbase = tf.constant(0.005) * tf.sin(tf.constant(2.0 * self.PI * self.fhi) * tf.to_float(t))

        theta = tf.atan2(y[1], y[0])
        dx_dt = a0 * y[0] - w0 * y[1]
        dy_dt = a0 * y[1] + w0 * y[0]

        b = tf.constant(2.0) * self.PI
        # d_theta_p = tf.mod(theta - self.theta[1], tf.constant(2.0 * self.PI))  # TODO: change mod
        a = theta - self.theta[1]
        c = a / b
        d_theta_p = a - tf.to_float(tf.to_int32(c)) * b
        delta_theta_2_p = d_theta_p * d_theta_p
        dz_dt_p = tf.constant(-1.0) * self.a[1] * d_theta_p * tf.exp(tf.constant(-0.5) * delta_theta_2_p /
                                                                     (self.b_adjusted_to_mean_heart_rate[1] *
                                                                      self.b_adjusted_to_mean_heart_rate[1]))

        # d_theta_q = tf.mod(theta - self.theta[2], tf.constant(2.0 * self.PI))  # TODO: change mod
        a = theta - self.theta[2]
        c = a / b
        d_theta_q = a - tf.to_float(tf.to_int32(c)) * b
        delta_theta_2_q = d_theta_q * d_theta_q
        dz_dt_q = tf.constant(-1.0) * self.a[2] * d_theta_q * tf.exp(tf.constant(-0.5) * delta_theta_2_q /
                                                                     (self.b_adjusted_to_mean_heart_rate[2] *
                                                                      self.b_adjusted_to_mean_heart_rate[2]))

        # d_theta_r = tf.mod(theta - self.theta[3], tf.constant(2.0 * self.PI))  # TODO: change mod
        a = theta - self.theta[3]
        c = a / b
        d_theta_r = a - tf.to_float(tf.to_int32(c)) * b
        delta_theta_2_r = d_theta_r * d_theta_r
        dz_dt_r = tf.constant(-1.0) * self.a[3] * d_theta_r * tf.exp(tf.constant(-0.5) * delta_theta_2_r /
                                                                     (self.b_adjusted_to_mean_heart_rate[3] *
                                                                      self.b_adjusted_to_mean_heart_rate[3]))

        # d_theta_s = tf.mod(theta - self.theta[4], tf.constant(2.0 * self.PI))  # TODO: change mod
        a = theta - self.theta[4]
        c = a / b
        d_theta_s = a - tf.to_float(tf.to_int32(c)) * b
        delta_theta_2_s = d_theta_s * d_theta_s
        dz_dt_s = tf.constant(-1.0) * self.a[4] * d_theta_s * tf.exp(tf.constant(-0.5) * delta_theta_2_s /
                                                                     (self.b_adjusted_to_mean_heart_rate[4] *
                                                                      self.b_adjusted_to_mean_heart_rate[4]))

        # d_theta_t = tf.mod(theta - self.theta[5], tf.constant(2.0 * self.PI))  # TODO: change mod
        a = theta - self.theta[5]
        c = a / b
        d_theta_t = a - tf.to_float(tf.to_int32(c)) * b
        delta_theta_2_t = d_theta_t * d_theta_t
        dz_dt_t = tf.constant(-1.0) * self.a[5] * d_theta_t * tf.exp(tf.constant(-0.5) * delta_theta_2_t /
                                                                     (self.b_adjusted_to_mean_heart_rate[5] *
                                                                      self.b_adjusted_to_mean_heart_rate[5]))

        dz_dt = dz_dt_p + dz_dt_q + dz_dt_r + dz_dt_s + dz_dt_t + tf.constant(-1.0) * (y[2] - zbase)
        return tf.Variable([dx_dt, dy_dt, dz_dt])

    def tf_ang_freq_calc(self, t):
        """
        THE ANGULAR FREQUENCY
        :param t:
        :return:
        """

        i = tf.constant(1) + tf.to_int32(tf.floor(tf.to_float(t) / tf.constant(self.h)))
        return tf.constant(2.0) * tf.constant(self.PI) / tf.to_float(self.rrpc_tf[i])
if __name__ == "__main__":
    ecg_app = ecg_sim()
    ecg_app.run_ecg_calcs()